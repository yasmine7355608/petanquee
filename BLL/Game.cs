using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL_Petanque
{
    public class Game
    {
        Team Team1 = new Team();
        Team Team2 = new Team();
        private int IntAantalGames;


        public Game()
        {
            
        }

        public int AantalGames
        {
            get { return IntAantalGames; }
            set { IntAantalGames = value; }
        }

        public string SetNamen(string strGegevenNaam1, string strGegevenNaam2)
        {
            Team1.Teamnaam = strGegevenNaam1;
            Team2.Teamnaam = strGegevenNaam2;
            return "Namen zijn doorgegeven";

            //Methode om de namen toe te kennen aan de teams (krijgt 2 namen binnen en geeft string metopmerking terug)
        }

        public string ResetScore()
        {
            Team1.Reset();
            Team2.Reset();
            return "De scores zijn reset";
        }

        //Methode om aantal winning games voor een team te verhogen met 1 (krijgt teamnummer binnen, geeft string met opmerking terug)
        public string verhoog(int GeselecteerdeTeam)
        {

            if(GeselecteerdeTeam == 1)
            {
                Team1.WinningGames = Team1.WinningGames + 1;
                return "succes";
            }
            else
            {
                Team2.WinningGames = Team2.WinningGames + 1;
                return "Succes";
            }
        }

        public int AantalGespeeldeGames()
        {
            IntAantalGames = Team1.WinningGames + Team2.WinningGames;
            return IntAantalGames;
            
        }

        public int AantalWinningGames(int GeselecteerdeTeam)     
        {
            if (GeselecteerdeTeam == 1)
            {
                return Team1.WinningGames;
                
            }
            else
            {

                return Team2.WinningGames;
            }
        }

        public string ReturnNaam(int GeselecteerdeTeam)
        {
            if (GeselecteerdeTeam == 1)
            {
                return Team1.Teamnaam;

            }
            else
            {

                return Team2.Teamnaam;
            }
        }

        public void verhoogScore(int intGeselecteerdeTeam, int intAantalVerhoogScore)
        {

            if (intGeselecteerdeTeam == 1)
            {
                Team1.Score = Team1.Score + intAantalVerhoogScore;
            }
            else if (intGeselecteerdeTeam == 2)
            {
                Team2.Score = Team2.Score + intAantalVerhoogScore;
            }
        }
    }
}
