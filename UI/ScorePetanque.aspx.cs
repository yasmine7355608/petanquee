using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL_Petanque;

namespace UI_Petanque
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        static Controller MijnController = new Controller();

        protected void btnTeam1Scoort_Click(object sender, EventArgs e)
        {
            /*
            if (MijnController.Team1.Score < 13)
            {   //  pas d'equipe gagnante
                MijnController.Team1.Score = MijnController.Team1.Score + Convert.ToInt32(txtbScoren1.Text);
            }
            else if (MijnController.Team2.Score < 13)
            {
                MijnController.Team2.Score = MijnController.Team2.Score + Convert.ToInt32(txtbScoren2.Text);
            }
            else
            {
                if(MijnController.Team1.Score == 13)
                {
                    MijnController.Team1.WinningGames = MijnController.Team1.WinningGames + 1;

                }
                else if (MijnController.Team2.Score == 13)
                {
                    MijnController.Team2.WinningGames = MijnController.Team2.WinningGames + 1;
                }

                MijnController.Team1.Score = 0;
                MijnController.Team2.Score = 0;
            }
            refresh();
            */
        }

        public void Refresh()
        {
            lblAantalGames.Text = Convert.ToString(MijnController.MijnGame.AantalGespeeldeGames());
            lblTeamnaam1.Text = MijnController.Team1.Teamnaam;
            lblTeamnaam2.Text = MijnController.Team2.Teamnaam;
            lblWinningGames1.Text = Convert.ToString(MijnController.Team1.WinningGames);
            lblWinningGames2.Text = Convert.ToString(MijnController.Team2.WinningGames);
            lblTussenScore1.Text = Convert.ToString(MijnController.Team1.Score);
            lblTussenScore2.Text = Convert.ToString(MijnController.Team2.Score);
        }

        protected void btnStart_Click(object sender, EventArgs e)
        {
            MijnController.MijnGame.AantalGames = 0;
            MijnController.Team1.Score = 0;
            MijnController.Team2.Score = 0;


            if(txtbNaam1.Text == "")
            {
                lblOpmerking.Text = "Gelieve een naam in te voegen";
            }
            else if (txtbNaam2.Text == "")
            {
                lblOpmerking.Text = "Gelieve een naam in te voegen";
            }
            else
            {
                MijnController.Team1.Teamnaam = txtbNaam1.Text;
                MijnController.Team2.Teamnaam = txtbNaam2.Text;
            }

            Refresh();

        }
    }
}
